var express = require("express");
var router = express.Router();
const mysql = require("mysql");

//DB Connection
const con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database: "urheilijatDB",
  multipleStatements: true,
});

//Connect to DB
con.connect((err) => {
  if (err) {
    console.log("Error connecting to Db");
    return;
  }
  console.log("DB Connection established");
});

// Default page
router.get("/", function (req, res) {
  res.send("Hello");
});

// CREATE, READ, EDIT, DELETE functions
// Get all urheilijat
router.get("/urheilijat", (req, res) => {
  con.query("SELECT * FROM urheilijat ORDER BY id DESC", function (
    error,
    results
  ) {
    if (error) throw error;
    res.json(results);
  });
});

// Get urheilija by id
router.get("/urheilijat/:id", (req, res) => {
  const id = req.params.id;
  con.query("SELECT * FROM urheilijat WHERE id = ?", id, function (
    error,
    results
  ) {
    if (error) throw error;
    res.json(results ? results : { message: "Not found" });
  });
});

// add urheilija
router.post("/urheilijat", (req, res) => {
  const urheilija = req.body; // example format object = { nimi: 'Ankka Roope', puhelin: '050-1231232' };
  console.log(urheilija);
  if (!urheilija) {
    return res
      .status(400)
      .send({ error: true, message: "Please provide urheilija" });
  }

  con.query("INSERT INTO urheilijat SET ?", urheilija, function (
    error,
    results
  ) {
    urheilija.id = results.insertId;

    if (error) throw error;
    return res.send({
      id: results.insertId,
      ...urheilija,
    });
  });
});

// update urheilija info
router.put("/urheilijat/:id", (req, res) => {
  const updatedUser = req.body;
  const id = Number(req.params.id);

  con.query(
    "UPDATE urheilijat SET ? Where ID = ?",
    [updatedUser, id],
    function (error, results) {
      if (error) throw error;
      return res.send({
        id: id,
        ...updatedUser,
      });
    }
  );
});

// delete urheilija
router.delete("/urheilijat/:id", (req, res) => {
  con.query("DELETE FROM urheilijat Where ID = ?", [req.params.id], function (
    error,
    results
  ) {
    if (error) throw error;
    return res.send({
      error: false,
      data: results,
      message: "urheilija deleted.",
    });
  });
});

module.exports = router;
