-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.11-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for urheilijatdb
CREATE DATABASE IF NOT EXISTS `urheilijatdb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `urheilijatdb`;

-- Dumping structure for table urheilijatdb.urheilijat
CREATE TABLE IF NOT EXISTS `urheilijat` (
  id int NOT NULL AUTO_INCREMENT,
  `etunimi` varchar(50) DEFAULT NULL,
  `sukunimi` varchar(50) DEFAULT NULL,
  `kutsumanimi` varchar(50) DEFAULT NULL,
  `syntymavuosi` datetime DEFAULT NULL,
  `paino` int(11) DEFAULT NULL,
  `kuvalinkki` varchar(255) DEFAULT NULL,
  `laji` varchar(50) DEFAULT NULL,
  `saavutukset` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table urheilijatdb.urheilijat: ~1 rows (approximately)
/*!40000 ALTER TABLE `urheilijat` DISABLE KEYS */;
INSERT INTO `urheilijat` (`etunimi`, `sukunimi`, `kutsumanimi`, `syntymavuosi`, `paino`, `kuvalinkki`, `laji`, `saavutukset`) VALUES
	('Reetta', 'Hurske', 'Reetta', '1995-05-13 00:00:00', 58, 'https://www.yleisurheilu.fi/wp-content/uploads/2019/11/1XA_5355.jpg', 'Aitajuoksu', '5. Sija alle 20-vuotiaiden EM-kilpailuissa 2013, 7. Sija alle 20-vuotiaiden MM-kilpailuissa 2014'),
	('Simo', 'Lipsanen', 'Simo', '1992-12-27 00:00:00', 72, 'https://www.yleisurheilu.fi/wp-content/uploads/2019/11/1XA_5284.jpg', 'Kolmiloikka', '3. sija 2015, EM-Hopeaa 2017, SM-kultaa 2015'),
	('Oliver', 'Helander', 'Oliver', '1997-01-01 00:00:00', 82, 'https://www.yleisurheilu.fi/wp-content/uploads/2019/11/1XA_5408.jpg', 'Keihäänheitto', '14-vuotiaiden Suomen ennätys 2011, 15-vuotiaiden Suomen ennätys 2012, 4. sija 18-vuotiaiden MM-kilpailussa 2013'),
	('Mari', 'Huntington', 'Maria', '1997-03-13 00:00:00', 61, 'https://www.yleisurheilu.fi/wp-content/uploads/2019/11/Huntington_Maria.jpg', 'Seitsenottelu', '13. Sijoitus EM-kilpailuissa 2015'),
	('Lassi', 'Etelätalo', 'Lassi', '1988-04-30 00:00:00', 90, 'https://www.yleisurheilu.fi/wp-content/uploads/2019/11/1XA_5404-2.jpg', 'Keihäänheitto', 'Kultaa 2019, hopeaa 2014, pronssia 2017'),
	('Veli-Matti', 'Partanen', 'Aku', '1991-10-28 00:00:00', 66, 'https://www.yleisurheilu.fi/wp-content/uploads/2019/11/SUL_DSC_3805.jpg', 'Kilpakävely', 'SM-kultaa 2013, 2018 ja 2019');
/*!40000 ALTER TABLE `urheilijat` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
