CREATE DATABASE IF NOT EXISTS urheilijatDB;

USE urheilijatDB;

CREATE TABLE IF NOT EXISTS urheilijat (
	id int NOT NULL AUTO_INCREMENT,
	etunimi VARCHAR(50),
	sukunimi VARCHAR(50),
	kutsumanimi VARCHAR(50),
	syntymavuosi DATETIME,
	paino INTEGER,
	kuvalinkki VARCHAR(255),
	laji VARCHAR(50),
	saavutukset VARCHAR(255),
	PRIMARY KEY(id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;